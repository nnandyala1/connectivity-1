package com.consumer.controller;

import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import com.tranformxml.XSLTTranformation;

@SpringBootApplication
@EnableJms
public class MessageRecieverApi extends SpringBootServletInitializer {
	private static final String DEFAULT_BROKER_URL = "tcp://devopsapp.southcentralus.cloudapp.azure.com:61616";

	private static final String request1 = "requestQ";

	@Bean
	public ActiveMQConnectionFactory connectionFactory() {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		connectionFactory.setBrokerURL(DEFAULT_BROKER_URL);
		connectionFactory.setTrustAllPackages(true);
		return connectionFactory;
	}

	@Bean(name = "jmsTemplate")
	// @Conditional(MessageConfiguration.class)
	public JmsTemplate jmsTemplate1() {
		JmsTemplate template = new JmsTemplate();
		template.setConnectionFactory(connectionFactory());
		template.setDefaultDestinationName(request1);
		return template;
	}

	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory());
		factory.setConcurrency("5-10");
		return factory;
	}
	
	//Used when run as JAR
	public static void main(String[] args) {
		SpringApplication.run(MessageRecieverApi.class, args);
	}
	
	// Used when run as WAR

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MessageRecieverApi.class);
	}

	@JmsListener(destination = "requestQ")
	public void jmsActive(String Jms) {
		System.out.println("jmsvalue......." +  Jms);
		XSLTTranformation.trasformXML(Jms);
	}

}
/*package com.consumer.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import com.tranformxml.XSLTTranformation;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

@SpringBootApplication
public class MessageRecieverApi extends SpringBootServletInitializer {

	 static MessageConsumer consumer;
	 static String xmlReceivedMessage;

	public static void messageReceiver() throws Exception {
		System.out.println("Called....");
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("admin", "admin",
				"tcp://devopsapp.southcentralus.cloudapp.azure.com:61616");
		Connection connection = connectionFactory.createConnection();
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination queue = session.createQueue("requestQ");
		consumer = session.createConsumer(queue);
		Message message = consumer.receive();
		while (message != null) {
			if (message instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) message;
				xmlReceivedMessage = textMessage.getText();
				System.out.println("Consume the msg :: \n" + xmlReceivedMessage);
				XSLTTranformation.trasformXML(xmlReceivedMessage);
			}
			message = consumer.receive();
		}
		connection.close();

	}

	// Used when run as JAR
	public static void main(String[] args) {
		SpringApplication.run(MessageRecieverApi.class, args);
		try {
			messageReceiver();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Used when run as WAR

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MessageRecieverApi.class);
	}

}*/