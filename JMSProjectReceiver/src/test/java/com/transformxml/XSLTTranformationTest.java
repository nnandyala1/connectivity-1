package com.transformxml;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.send.jms.QueueSenderAfterHazelCast;
import com.tranformxml.MyErrorListener;
import com.tranformxml.XSLTTranformation;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ XSLTTranformation.class, MyErrorListener.class, QueueSenderAfterHazelCast.class })
public class XSLTTranformationTest {

	@Mock
	TransformerException te;
	
	@Mock
	TransformerConfigurationException tce;
	
	@Mock
	XSLTTranformation xt;
	
	
	@Test
	public void testXSLTTranformationExceptionCase(){
		String in = "<Employee><FirstName>Ajay</FirstName><LastName>Kumar</LastName><Id>512</Id></Employee>";
		String inXSL = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
				+ "<xsl:output method=\"xml\" indent=\"yes\" />"
				+ "<xsl:template match=\"Employee\">"
				+ "<xsl:copy><xsl:copy-of select=\"Employee\" />"
				+ "<Name><xsl:value-of select=\"concat(FirstName, '_', LastName)\" /></Name>"
				+ "<Id><xsl:value-of select=\"Id\" /></Id>"
				+ "</xsl:copy></xsl:template></xsl:stylesheet>";
		String out = "";
		try{
			xt = PowerMockito.mock(XSLTTranformation.class);
			PowerMockito.whenNew(XSLTTranformation.class).withNoArguments().thenReturn(xt);
			//PowerMockito.mockStatic(XSLTTranformation.class);
			PowerMockito.when(new XSLTTranformation(), "transform", in,inXSL, out).thenThrow(tce);
			XSLTTranformation.trasformXML(in);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			xt = PowerMockito.mock(XSLTTranformation.class);
			PowerMockito.whenNew(XSLTTranformation.class).withNoArguments().thenReturn(xt);
			//PowerMockito.mockStatic(XSLTTranformation.class);
			PowerMockito.when(new XSLTTranformation(), "transform", in,inXSL, out).thenThrow(te);
			XSLTTranformation.trasformXML(in);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		 
		
	}

	@Test
	public void testXSLTTranformationNoExceptionCase() {
		try {
			PowerMockito.mockStatic(QueueSenderAfterHazelCast.class);
			PowerMockito.doNothing().when(QueueSenderAfterHazelCast.class, "afterXSLT", "dummy1", "dummy2");

			XSLTTranformation.trasformXML(
					"<Employee><FirstName>Ajay</FirstName><LastName>Kumar</LastName><Id>512</Id></Employee>");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testMyErrorListener() {
		te = PowerMockito.mock(TransformerException.class);
		PowerMockito.when(te.getLocationAsString()).thenReturn("Dummy Message..");
		MyErrorListener mel = new MyErrorListener();
		try {

			mel.fatalError(te);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			mel.warning(te);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			mel.error(te);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
